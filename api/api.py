# Import statements
# =================
import requests
import random
import json
from requests.auth import HTTPBasicAuth


# Common input parameters
# =======================
BASE_URL            = "http://localhost:8080"
END_POINT_AUTH      = "/api/auth/token"
END_POINT_USERS     = "/api/users"
END_POINT_USER      = "/api/users/"
END_POINT_REGISTER  = "/register"

def get_token(username, password):
    s = requests.Session()
    s.auth = HTTPBasicAuth (username, password)
    s.headers.update({'Content-Type': 'application/json'})
    url = BASE_URL + END_POINT_AUTH
    response = s.get(url)
    return response

def get_user(username, password):
    s = requests.Session()
    s.auth = HTTPBasicAuth (username, password)
    tokenresponse = get_token(username, password)
    if (tokenresponse.status_code != 200):
        return tokenresponse
    data = tokenresponse.json()
    token = data["token"]
    s.headers.update({'Token': token, 'Content-Type': 'application/json'})
    url = BASE_URL + END_POINT_USER + username
    response = s.get(url)
    return response

def get_users():
    s = requests.Session()
    url = BASE_URL + END_POINT_USERS
    response = s.get(url)
    return response

def set_user(username, password, data):
    url = BASE_URL + END_POINT_REGISTER
    response = requests.post(url, data)
    return response

def update_user(username, password, payload):
    s = requests.Session()
    s.auth = HTTPBasicAuth (username, password)
    tokenresponse = get_token(username, password)
    if (tokenresponse.status_code != 200):
        return tokenresponse
    data = tokenresponse.json()
    token = data["token"]
    s.headers.update({'Token': token, 'Content-Type': 'application/json'})
    url = BASE_URL + END_POINT_USER + username
    response = s.put(url, data=json.dumps(payload))
    return response
     
