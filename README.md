# Customer: Demo app
## Test Information
```bash
Test Type:              Api testing
Test Coverage:          1: Review users registered in system
                            a. Execute API call with endpoint /api/users
                            b. Register user with http://localhost:8080/register

                        2: If authenticated I can get personal information of users
                            a. Get access token for a registered user
                            b. Get a registered user with username and password
                            c. Get invalid user should fail 

                        3: If authenticated I can update personal information of users
                            a. Update a registered user
                            b. Update an invalid user should fail

Automation Test tool:   python framework

Installation:           Install python and pip
                        pip install pytest
                        pip install unittest
                        pip install pytest-html - for report.html file
                        Pip install pytest-ordering - for ordering the tests
                        pip install coverage - Static testing tool

Test Execution:         Clone the project repository flaskyapis into a <directory>
                        cd <directory>
                        Run command
                        coverage run -m pytest -v --html=report.html test_api.py
                        coverage html <for coverage report in report.html file 

Test Results:           
    1. Results can be found in report.html and file in flaskyapis/reports directory and coverage report can be found in flaskyapis/htmlcov directory
```
