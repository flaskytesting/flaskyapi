# Import statements
# =================
import unittest
import pytest
import requests
import random
import json
from requests.auth import HTTPBasicAuth
from api.api import get_token
from api.api import get_user
from api.api import get_users
from api.api import set_user
from api.api import update_user


# Common input parameters
# =======================
BASE_URL            = "http://localhost:8080"
END_POINT_AUTH      = "/api/auth/token"
END_POINT_USERS     = "/api/users"
END_POINT_USER      = "/api/users/"
username             = "abcd"
password             = "abcd"    

# Base classes
# ============
class Base(unittest.TestCase):

    """
    Base class for tests
    This class defines a common `setUp` method that defines attributes which are used in the various tests.
    """
    @classmethod
    def setUpClass(cls):
        """
        suite level setup
        Assign random values for user payload
        """
        cls.userdict = dict()
        characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'
        randomstring = ''
        for i in range(0, 6):
            randomstring += random.choice(characters)
        cls.userdict["firstname"] = randomstring
        randomstring = ''
        for i in range(0, 6):
            randomstring += random.choice(characters)
        cls.userdict["lastname"] = randomstring
        characters = '0123456789'
        randomstring = ''
        for i in range(0, 9):
            randomstring += random.choice(characters)
        cls.userdict["phone"] = randomstring
        print(cls.userdict)

    @classmethod
    def tearDownClass(cls):
        """
        Suite tear down
        """
        print ("suite level tear down")
    
    def setUp(self):
        """
        Test level setUp
        Start session for api testing
        """
        print ("case level setup")
        self.session = requests.session()

        self.registerdict = dict()
        characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_'
        randomstring = ''
        for i in range(0, 6):
            randomstring += random.choice(characters)
        self.registerdict["username"] = randomstring
        randomstring = ''
        for i in range(0, 6):
            randomstring += random.choice(characters)
        self.registerdict["password"] = randomstring
        

    def tearDown(self):
        """
        Test tear down
        """
        print ("case level tear down")

    @pytest.mark.run(order=1)
    def test_get_users(self):
        """
        Get all users in Demo App
        Users exist
        """
        response = get_users()
        self.assertEqual (response.status_code, 200)
        
    
    @pytest.mark.run(order=2)
    def test_register_new_user(self):
        """
        New user created
        New user fetched and verified against input data set
        """
        registerdata = self.registerdict
        username    = registerdata["username"]
        password    = registerdata["password"]
        data = {"username": username, "password": password, "firstname": "Myfirstname", "lastname": "Mylastname", "phone": "999555777"}
        response = set_user(username, password, data)
        self.assertEqual (response.status_code, 200)
        response = get_user(username, password)
        self.assertEqual (response.status_code, 200)
        data = response.json()
        self.assertEqual (data["payload"]["firstname"], "Myfirstname")
        self.assertEqual (data["payload"]["lastname"], "Mylastname")
        self.assertEqual (data["payload"]["phone"], "999555777")
    
    @pytest.mark.run(order=3)
    def test_auth(self):
        """
        Get users authorization token using username and password
        """
        registerdata = self.registerdict
        username    = registerdata["username"] 
        password    = registerdata["password"]
        data = {"username": username, "password": password, "firstname": "Myfirstname", "lastname": "Mylastname", "phone": "999555777"}
        response = set_user(username, password, data)
        self.assertEqual (response.status_code, 200)

        response = get_token(username, password)
        self.assertEqual (response.status_code, 200)
        data = response.json()
        self.assertEqual (data["status"], "SUCCESS")
    
    @pytest.mark.run(order=4)
    def test_get_user(self):
        """
        Get an existing user with authorization token and username
        User exists
        """
        registerdata = self.registerdict
        username    = registerdata["username"]
        password    = registerdata["password"]
        data = {"username": username, "password": password, "firstname": "Myfirstname", "lastname": "Mylastname", "phone": "999555777"}
        response = set_user(username, password, data)
        self.assertEqual (response.status_code, 200)
    
        response = get_user(username, password)
        self.assertEqual (response.status_code, 200)
        data = response.json()
        self.assertEqual (data["payload"]["firstname"], "Myfirstname")
        self.assertEqual (data["payload"]["lastname"], "Mylastname")
        self.assertEqual (data["payload"]["phone"], "999555777")

    @pytest.mark.run(order=5)
    def test_update_user(self):
        """
        Add a new user
        Update the user's details
        Get updated user details
        Verify fetched user details against updated one
        """
        registerdata = self.registerdict
        username    = registerdata["username"]
        password    = registerdata["password"]
        data = {"username": username, "password": password, "firstname": "Myfirstname", "lastname": "Mylastname", "phone": "999555777"}
        response = set_user(username, password, data)
        self.assertEqual (response.status_code, 200)
        payload  = self.userdict
        response = update_user(username, password, payload)
        self.assertEqual (response.status_code, 201)
        data = response.json()
        self.assertEqual (data["message"], "Updated")
        response = get_user(username, password)
        self.assertEqual (response.status_code, 200)
        data = response.json()
        self.assertEqual (data["payload"], payload)

    @pytest.mark.run(order=6)
    def test_update_invalid_user(self):
        """
        Update an existing user's details
        Get updated user details
        Verify fetched user details against updated one
        """
        username = "invalid_user"
        password = "invalid_password"
        payload  = self.userdict
        response = update_user(username, password, payload)
        self.assertEqual (response.status_code, 401)
        data = response.json()
        self.assertEqual (data["message"], 'Invalid User')

    @pytest.mark.run(order=7)
    def test_get_invalid_user_fail(self):
        """
        Get an invalid user
        No token exists for the user
        """
        username    = "invalid-name"
        password    = "invalid-password"
        response = get_user(username, password)
        self.assertEqual (response.json()["message"], 'Invalid User')   
        self.assertEqual (response.json()["status"], 'FAILURE')